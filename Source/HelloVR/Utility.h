// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CoreMinimal.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Utility.generated.h"

class UStaticMesh;
class UStaticMeshComponent;
class UProceduralMeshComponent;


/**
 * 
 */
UCLASS()
class HELLOVR_API Utility : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	
	UFUNCTION(BlueprintPure, Category = "Utility", meta = (Keywords = "Drawer"))
		static FVector Drawer(float maxDistance, FVector grabStartLocation, FVector grabCurrentLocation, FVector drawerStartLocation, FVector currentDrawerLocation, FVector drawerForward, float speed = 1.0f);
	UFUNCTION(BlueprintPure, Category = "Utility", meta = (Keywords = "OvenDoor"))
		static FRotator OvenDoor(FRotator currentRotation, float maxAngle, FVector grabCurrentLocation, FVector ovenRotPosition, FVector ovenHandlePosition, FVector ovenHandleForward, float speed = 1.0f);
	UFUNCTION(BlueprintPure, Category = "Utility", meta = (Keywords = "FridgeDoor"))
		static FRotator FridgeDoor(FRotator currentRotation, float maxAngle, FVector grabCurrentLocation, FVector fridgeRotPosition, FVector fridgeHandlePosition, FVector fridgeHandleForward, float speed = 1.0f);
	UFUNCTION(BlueprintCallable, Category = "Utility", meta = (Keywords = "Take"))
		static void PickUp(UStaticMeshComponent* visualTarget, USceneComponent* physicalTarget, USceneComponent* attachTo);
	UFUNCTION(BlueprintCallable, Category = "Utility", meta = (Keywords = "Give"))
		static void Drop(UStaticMeshComponent* visualTarget);
	//UFUNCTION(BlueprintCallable, Category = "Utility", meta = (Keywords = "Take"))
	//	static void PickUpProcedural(UProceduralMeshComponent* visualTarget, USceneComponent* physicalTarget, USceneComponent* attachTo);
	//UFUNCTION(BlueprintCallable, Category = "Utility", meta = (Keywords = "Give"))
	//	static void DropProcedural(UProceduralMeshComponent* visualTarget);
	UFUNCTION(BlueprintCallable, Category = "Utility", meta = (Keywords = "Spin"))
		static void Spin(USceneComponent* player, float spinSpeed, float angle, float& spinSpeedOut, float& angleOut, bool& spinsOut, bool& releaseGrabOut);
	UFUNCTION(BlueprintPure, Category = "Utility", meta = (Keywords = "IsSharp"))
		static bool IsSharpSide(UPrimitiveComponent* sharp, UPrimitiveComponent* blunt, UPrimitiveComponent* otherComponent);
	UFUNCTION(BlueprintCallable, Category = "Utility", meta = (WorldContext = "WorldContextObject", AutoCreateRefTerm = "ActorsToIgnore", Keywords = "IsHeating"))
		static bool IsHeating(UObject* WorldContextObject, USceneComponent* pan, const TArray<TEnumAsByte<EObjectTypeQuery> > & objectTypes, int checkDistance, FHitResult& OutHit, bool bTraceComplex, const TArray<AActor*>& ActorsToIgnore, EDrawDebugTrace::Type DrawDebugType, FLinearColor TraceColor = FLinearColor::Red, FLinearColor TraceHitColor = FLinearColor::Green, float DrawTime = 5.0f);
	UFUNCTION(BlueprintPure, Category = "Utility", meta = (Keywords = "ChangeHeat", ToolTip = "minus values to reduce heat"))
		static float ChangeHeat(float heat, float heatInput, float maxHeat, float deltaTime);
	UFUNCTION(BlueprintPure, Category = "Utility", meta = (Keywords = "Mix"))
		static FLinearColor MixingFluids(FLinearColor oldColor, float newR, float newG, float newB, float newA, float newVolume, float totalVolume);
	UFUNCTION(BlueprintPure, Category = "Utility", meta = (Keywords = "Pour"))
		static float Pouring(FVector upVector);
	UFUNCTION(BlueprintPure, Category = "Utility", meta = (Keywords = "Toggle"))
		static bool ToggleBoolean(bool boolToToggle);
	UFUNCTION(BlueprintCallable, Category = "Utility", meta = (Keywords = "SetRoot"))
		static void SetRootComponent(AActor* target, USceneComponent* newRoot);
	UFUNCTION(BlueprintPure, Category = "Utility", meta = (Keywords = "Pour"))
		static float GetAngleBetweenActor(FVector upVectorA, FVector upVectorB);
	//UFUNCTION(BlueprintCallable, Category = "Utility")
	//	static void GetLowestLocation(UStaticMeshComponent* mesh, FVector& VertexPosition);

private:
	//static FCollisionQueryParams ConfigureCollisionParams(FName TraceTag, bool bTraceComplex, const TArray<AActor*>& ActorsToIgnore, bool bIgnoreSelf, UObject* WorldContextObject);
	//static FCollisionObjectQueryParams ConfigureCollisionObjectParams(const TArray<TEnumAsByte<EObjectTypeQuery> > & ObjectTypes);
	//static void DrawDebugLineTraceSingle(const UWorld* World, const FVector& Start, const FVector& End, EDrawDebugTrace::Type DrawDebugType, bool bHit, FHitResult& OutHit, FLinearColor TraceColor, FLinearColor TraceHitColor, float DrawTime);
};
