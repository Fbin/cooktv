// Fill out your copyright notice in the Description page of Project Settings.

#include "CookGameStateBase.h"
#include "Kismet/KismetArrayLibrary.h"
#include "BaseIngredients.h"

UCookGameStateBase* UCookGameStateBase::Get()
{
	UCookGameStateBase* DataInstance = Cast<UCookGameStateBase>(GEngine->GameSingleton);

	return DataInstance;
}

//int UCookGameStateBase::GetAmount(ABaseIngredients* ingredient)
//{
//	int length = ingredients.Num();
//	int count = 0;
//	for (int i = 0; i < length; i++)
//	{
//		if (ingredients[i] == NULL)
//		{
//			//UArrayProperty* prop = ingredient->GetClass()->property//UArrayProperty(ECppProperty::EC_CppProperty, int32 InOffset, uint64 InFlags);
//			//UKismetArrayLibrary::GenericArray_RemoveItem(ingredients, ingredient);
//			ingredients.RemoveAtSwap(i);
//		}
//		if (ingredient->GetLClass() == ingredients[i]->GetLClass())
//		{
//			count++;
//		}
//	}
//	return count;
//}

void UCookGameStateBase::addItem(AActor* object)
{
	resetLocations.Add(FResetStruct(object));
}

void UCookGameStateBase::removeItem(AActor* object)
{
	resetLocations.RemoveSingle(FResetStruct(object));
}

void UCookGameStateBase::addIngredient(ABaseIngredients* object)
{
	ingredients.Add(object);
}

void UCookGameStateBase::removeIngredient(ABaseIngredients* object)
{
	//UKismetArrayLibrary::Array_RemoveItem(ingredients, object);
	ingredients.RemoveSingleSwap(object);
}