// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseIngredients.h"
#include "CookGameStateBase.h"

void ABaseIngredients::BeginPlay()
{
	Super::BeginPlay();
	if (material == NULL)
	{
		material = StaticMeshComponent->GetMaterial(0);
	}
	baseMaterial = UMaterialInstanceDynamic::Create(material, this);
	bakingMaterial = UMaterialInstanceDynamic::Create(baseBakeMaterial, this);
	boilingMaterial = UMaterialInstanceDynamic::Create(baseBoilMaterial, this);
	roastingMaterial = UMaterialInstanceDynamic::Create(baseRoastMaterial, this);
	overBakedMaterial = UMaterialInstanceDynamic::Create(baseOverbakedMaterial, this);
	overBoiledMaterial = UMaterialInstanceDynamic::Create(baseOverboiledMaterial, this);
	overRoastedMaterial = UMaterialInstanceDynamic::Create(baseBurntMaterial, this);
	//UE_LOG(LogTemp, Warning, TEXT("[Game] BaseColor final: %s"), *temp->K2_GetVectorParameterValue("Base Color").ToString());
	
	bakingMaterial->SetVectorParameterValue("Base Color", baseMaterial->K2_GetVectorParameterValue("None"));
	boilingMaterial->SetVectorParameterValue("Base Color", baseMaterial->K2_GetVectorParameterValue("None"));
	roastingMaterial->SetVectorParameterValue("Base Color", baseMaterial->K2_GetVectorParameterValue("None"));
	overBakedMaterial->SetVectorParameterValue("Base Color", baseMaterial->K2_GetVectorParameterValue("None"));
	overBoiledMaterial->SetVectorParameterValue("Base Color", baseMaterial->K2_GetVectorParameterValue("None"));
	overRoastedMaterial->SetVectorParameterValue("Base Color", baseMaterial->K2_GetVectorParameterValue("None"));
	
	if (StaticMeshComponent)
	{
		//StaticMeshComponent->SetMaterial(0, baseMaterial);
		StaticMeshComponent->BodyInstance.bUseCCD = true;
	}

	lClass = GetClass();
	if (GetLClass() == NULL)
	{
		Destroy();
	}
	UCookGameStateBase* gamestateRef = UCookGameStateBase::Get();
	gamestateRef->addIngredient(this);
}

void ABaseIngredients::PostInitProperties()
{
	Super::PostInitProperties();
	//staticMesh->SetStaticMesh(visualMesh);
	if(StaticMeshComponent) StaticMeshComponent->SetSimulatePhysics(true);
}

void ABaseIngredients::Destroyed()
{
	Super::Destroyed();
	UCookGameStateBase* gamestateRef = UCookGameStateBase::Get();
	gamestateRef->removeIngredient(this);
}

UClass* ABaseIngredients::GetLClass()
{
	return lClass;
}