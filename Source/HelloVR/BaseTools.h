// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseObject.h"
#include "BaseTools.generated.h"

class ABaseIngredients;

UENUM(BlueprintType)
enum class EToolType : uint8
{
	Bottle 	UMETA(DisplayName = "Bottle"),
	Not 	UMETA(DisplayName = "Not"),
};

USTRUCT(BlueprintType)
struct FResetStruct
{
	GENERATED_USTRUCT_BODY()

	FResetStruct()
	{
		object = NULL;
		startTransform = FTransform();
	}

	FResetStruct(AActor* newObject)
	{
		object = newObject;
		startTransform = newObject->GetActorTransform();
	}

	UPROPERTY(BlueprintReadWrite)
		AActor* object;
	UPROPERTY(BlueprintReadWrite)
		FTransform startTransform;
public:
	bool operator==(const FResetStruct& other) const
	{
		return object == other.object;
	}
};
/**
 * 
 */
UCLASS()
class HELLOVR_API ABaseTools : public ABaseObject
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
		EToolType isBottle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
		TArray<ABaseIngredients*> ingredients;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mixing)
		float volume;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mixing)
		float newVolume;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mixing)
		float newR;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mixing)
		float newG;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mixing)
		float newB;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mixing)
		float newA;
	UFUNCTION(BlueprintCallable, Category = "BaseTools", meta = (WorldContext = "WorldContextObject", AutoCreateRefTerm = "ActorsToIgnore"))
		FVector GetLowestLocation(FTransform objctTransform, UObject* WorldContextObject, float Radius);
	UFUNCTION(BlueprintCallable, Category = "BaseTools", meta = (WorldContext = "WorldContextObject", AutoCreateRefTerm = "ActorsToIgnore"))
		FVector Boiling(FVector locationIn, FTransform potTransform, UObject* WorldContextObject, float range, float boilingState);
protected:
	virtual void PostInitProperties() override;
	virtual void BeginPlay() override;
	virtual void Destroyed() override;
};
