// Fill out your copyright notice in the Description page of Project Settings.

#include "Utility.h"
#include "HelloVR.h"
#include "Components/PrimitiveComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

#include "PhysicsEngine/PhysicsSettings.h"
//#include "KismetTraceUtils.h"
//#include "PhysXIncludes.h"
//#include "PhysXPublic.h"

#define DO_DEBUG_TRACE

FVector Utility::Drawer(float maxDistance, FVector grabStartLocation, FVector grabCurrentLocation, FVector drawerStartLocation, FVector currentDrawerLocation, FVector drawerForward, float speed)
{
	FVector move = grabCurrentLocation - grabStartLocation;
	FVector projectedMove = move.ProjectOnTo(drawerForward);

	FVector previousMove = currentDrawerLocation - drawerStartLocation;
	if (FVector::DotProduct(move.GetSafeNormal(),drawerForward) <= 0)
	{
		float distance = previousMove.Size() - projectedMove.Size();
		if (distance<0)
		{
			return drawerStartLocation;
		}
		return drawerStartLocation + drawerForward*distance;
	}
	else {
		float distance = projectedMove.Size() + previousMove.Size();
		if (distance > maxDistance)
		{
			return drawerStartLocation + drawerForward*maxDistance;
		}
		return drawerStartLocation + drawerForward*distance*speed;
	}
}

FRotator Utility::OvenDoor(FRotator currentRotation, float maxAngle, FVector grabCurrentLocation, FVector ovenRotPosition, FVector ovenHandlePosition, FVector ovenHandleForward, float speed)
{
	float dAngle;
	FRotator newRot;
	FVector door = ovenHandlePosition - ovenRotPosition;
	FVector move = (grabCurrentLocation - ovenHandlePosition).ProjectOnTo(ovenHandleForward);
	float angle = FMath::Atan(move.Size() / door.Size());
	if (FVector::DotProduct(move, ovenHandleForward) <= 0)
	{
		dAngle = angle*speed;
	}
	else
	{
		dAngle = -angle*speed;
	}

	newRot = currentRotation + FRotator(dAngle, 0.0f, 0.0f);
	if (newRot.Pitch < -maxAngle)
	{
		newRot.Pitch = -maxAngle;
	}
	if (newRot.Pitch > 0.0f)
	{
		newRot.Pitch = 0.0f;
	}

	return newRot;
}

FRotator Utility::FridgeDoor(FRotator currentRotation, float maxAngle, FVector grabCurrentLocation, FVector fridgeRotPosition, FVector fridgeHandlePosition, FVector fridgeHandleForward, float speed)
{
	float dAngle;
	FRotator newRot;
	FVector door = fridgeHandlePosition - fridgeRotPosition;
	FVector move = (grabCurrentLocation - fridgeHandlePosition).ProjectOnTo(fridgeHandleForward);
	float angle = FMath::Atan(move.Size() / door.Size());
	if (FVector::DotProduct(move, fridgeHandleForward) <= 0)
	{
		dAngle = angle*speed;
	}
	else
	{
		dAngle = -angle*speed;
	}

	newRot = currentRotation + FRotator(0.0f, dAngle, 0.0f);
	if (newRot.Yaw < -maxAngle)
	{
		newRot.Yaw = -maxAngle;
	}
	if (newRot.Yaw > 0.0f)
	{
		newRot.Yaw = 0.0f;
	}

	return newRot;
}

void Utility::PickUp(UStaticMeshComponent * visualTarget, USceneComponent * physicalTarget, USceneComponent * attachTo)
{
	UPrimitiveComponent* visualTargetPC = Cast<UPrimitiveComponent>(visualTarget);
	visualTargetPC->SetSimulatePhysics(false);
	FAttachmentTransformRules rules = FAttachmentTransformRules(EAttachmentRule::KeepWorld, false);
	physicalTarget->AttachToComponent(attachTo, rules);
}

//void Utility::PickUpProcedural(UProceduralMeshComponent* visualTarget, USceneComponent* physicalTarget, USceneComponent* attachTo)
//{
//	UPrimitiveComponent* visualTargetPC = Cast<UPrimitiveComponent>(visualTarget);
//	visualTargetPC->SetSimulatePhysics(false);
//	FAttachmentTransformRules rules = FAttachmentTransformRules(EAttachmentRule::KeepWorld, false);
//	physicalTarget->AttachToComponent(attachTo, rules);
//};

void Utility::Drop(UStaticMeshComponent* visualTarget)
{
	UPrimitiveComponent* cast = Cast<UPrimitiveComponent>(visualTarget);
	cast->SetSimulatePhysics(true);
	AActor* owner = cast->GetOwner();
	FDetachmentTransformRules rules = FDetachmentTransformRules(EDetachmentRule::KeepWorld, false);
	owner->DetachFromActor(rules);
}

//void Utility::DropProcedural(UProceduralMeshComponent* visualTarget)
//{
//	UPrimitiveComponent* cast = Cast<UPrimitiveComponent>(visualTarget);
//	cast->SetSimulatePhysics(true);
//	AActor* owner = cast->GetOwner();
//	FDetachmentTransformRules rules = FDetachmentTransformRules(EDetachmentRule::KeepWorld, false);
//	owner->DetachFromActor(rules);
//};

void Utility::Spin(USceneComponent* player, float spinSpeed, float angle, float& spinSpeedOut, float& angleOut, bool& spinsOut, bool& releaseGrabOut)
{
	float yaw = angle + spinSpeed / 60;
	FRotator rotplayer=player->GetComponentRotation();
	rotplayer = rotplayer + FRotator(0, yaw, 0);
	player->SetWorldRotation(rotplayer);
	angleOut = yaw;
	if (fabs(spinSpeed - spinSpeed / 60) < 60)
	{
		spinSpeedOut = 0.0;
		spinsOut = false;
		releaseGrabOut = true;
	}
	else
	{
		spinSpeedOut = spinSpeed - spinSpeed / 60;
	}
}

bool Utility::IsSharpSide(UPrimitiveComponent* sharp, UPrimitiveComponent* blunt, UPrimitiveComponent* otherComponent)
{
	return sharp->IsOverlappingComponent(otherComponent) && !blunt->IsOverlappingComponent(otherComponent);
}

bool Utility::IsHeating(UObject* WorldContextObject, USceneComponent* pan, const TArray<TEnumAsByte<EObjectTypeQuery> > & objectTypes, int checkDistance, FHitResult& OutHit, bool bTraceComplex, const TArray<AActor*>& ActorsToIgnore, EDrawDebugTrace::Type DrawDebugType, FLinearColor TraceColor, FLinearColor TraceHitColor, float DrawTime)
{
	FVector start = pan->GetComponentLocation();
	FVector end = pan->GetComponentLocation() - pan->GetUpVector()*((float)checkDistance);
	
	static const FName LineTraceSingleName(TEXT("LineTraceSingleForObjects"));
	FCollisionQueryParams Params(LineTraceSingleName, bTraceComplex);
	Params.bReturnPhysicalMaterial = true;
	Params.bReturnFaceIndex = !UPhysicsSettings::Get()->bSuppressFaceRemapTable; // Ask for face index, as long as we didn't disable globally
	Params.bTraceAsyncScene = true;
	Params.AddIgnoredActors(ActorsToIgnore);
	AActor* IgnoreActor = Cast<AActor>(WorldContextObject);
	if (IgnoreActor)
	{
		Params.AddIgnoredActor(IgnoreActor);
	}
	else
	{
		UObject* CurrentObject = WorldContextObject;
		while (CurrentObject)
		{
			CurrentObject = CurrentObject->GetOuter();
			IgnoreActor = Cast<AActor>(CurrentObject);
			if (IgnoreActor)
			{
				Params.AddIgnoredActor(IgnoreActor);
				break;
			}
		}
	}
	
	//FCollisionQueryParams Params = ConfigureCollisionParams(LineTraceSingleName, bTraceComplex, ActorsToIgnore, true, WorldContextObject);

	TArray<TEnumAsByte<ECollisionChannel>> CollisionObjectTraces;
	CollisionObjectTraces.AddUninitialized(objectTypes.Num());

	for (auto Iter = objectTypes.CreateConstIterator(); Iter; ++Iter)
	{
		CollisionObjectTraces[Iter.GetIndex()] = UEngineTypes::ConvertToCollisionChannel(*Iter);
	}

	FCollisionObjectQueryParams ObjectParams;
	for (auto Iter = CollisionObjectTraces.CreateConstIterator(); Iter; ++Iter)
	{
		const ECollisionChannel & Channel = (*Iter);
		if (FCollisionObjectQueryParams::IsValidObjectQuery(Channel))
		{
			ObjectParams.AddObjectTypesToQuery(Channel);
		}
		else
		{
			UE_LOG(LogBlueprintUserMessages, Warning, TEXT("%d isn't valid object type"), (int32)Channel);
		}
	}
	//FCollisionObjectQueryParams ObjectParams = ConfigureCollisionObjectParams(objectTypes);
	if (ObjectParams.IsValid() == false)
	{
		UE_LOG(LogBlueprintUserMessages, Warning, TEXT("Invalid object types"));
		return false;
	}

	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	bool const bHit = World->LineTraceSingleByObjectType(OutHit, start, end, ObjectParams, Params);

//#ifdef DO_DEBUG_TRACE
//	DrawDebugLineTraceSingle(World, start, end, DrawDebugType, bHit, OutHit, TraceColor, TraceHitColor, DrawTime);
//#endif

	return bHit;
}

float Utility::ChangeHeat(float heat, float heatInput, float maxHeat, float deltaTime)
{
	float v = heat + heatInput * deltaTime;
	return FMath::Clamp(v, 0.0f, maxHeat);
}

FLinearColor Utility::MixingFluids(FLinearColor oldColor, float newR, float newG, float newB, float newA, float newVolume, float totalVolume)
{
	totalVolume = FMath::Max(totalVolume, 0.1f);
	float relativVol = newVolume / totalVolume;
	if (totalVolume == 0)
	{
		relativVol = 0;
	}
	if (relativVol > 1)
	{
		relativVol = 1;
	}
	FLinearColor newColor = FLinearColor(
		(oldColor.R * (1-relativVol) + (newR * relativVol)), 
		(oldColor.G * (1-relativVol) + (newG * relativVol)), 
		(oldColor.B * (1 - relativVol) + (newB * relativVol)),
		(oldColor.A * (1 - relativVol) + (newA * relativVol)));
	return newColor;
}

float Utility::Pouring(FVector upVector)
{
	if (asin(upVector.Z / upVector.Size())*180/PI > 85)
	{
		return 1.0f;
	}
	if (asin(upVector.Z / upVector.Size()) * 180 / PI < 0)
	{
		return 0.0f;
	}
	return fabs(asin(upVector.Z / upVector.Size())) * 180 / PI / 90;
}

bool Utility::ToggleBoolean(bool boolToToggle)
{
	return !boolToToggle;
}

void Utility::SetRootComponent(AActor* target, USceneComponent* newRoot)
{
	//newRoot->AttachParent = target;
	target->SetRootComponent(newRoot);
}

float Utility::GetAngleBetweenActor(FVector upVectorA, FVector upVectorB)
{
	return asin(FVector::DotProduct(upVectorA, upVectorB) / (upVectorA.Size() * upVectorB.Size())) * 180 / PI;
}

//void Utility::GetLowestLocation(UStaticMeshComponent* mesh, FVector& VertexPosition)
//{
//	TArray<FVector> positions;
//	FTransform transform = mesh->GetComponentTransform();
//	UBodySetup* bodySetup = mesh->GetBodySetup();
//
//	for (PxTriangleMesh* EachTriMesh : bodySetup->TriMeshes)
//	{
//		if (!EachTriMesh)
//		{
//			abort;
//		}
//
//		//PxU32 VertexCount = EachTriMesh->getNbVertices();
//		int32 VertexCount = EachTriMesh->getNbVertices();
//
//		const PxVec3* Vertices = EachTriMesh->getVertices();
//
//		//for (PxU32 v = 0; v < VertexCount; v++)
//		for (int32 v = 0; v < VertexCount; v++)
//		{
//			positions.Add(transform.TransformPosition(P2UVector(Vertices[v])));
//		}
//	}
//	//FBoxSphereBounds bounds = mesh->GetBounds();
//	//FVector min = bounds.Origin - bounds.BoxExtent;
//}