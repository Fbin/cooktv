// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseIngredients.h"
#include "ProceduralActor.generated.h"

class UProceduralMeshComponent;

UCLASS()
class HELLOVR_API AProceduralActor : public ABaseIngredients
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProceduralActor(const FObjectInitializer& ObjectInitializer);
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void PostInitProperties() override;
	virtual void BeginPlay() override;

public:	
	// Called every frame
	//virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
		UMaterialInterface* capMaterial;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
		UProceduralMeshComponent* ProceduralMesh;
	UFUNCTION(BlueprintCallable, Category = "ProceduralActor")
		void ReRoot(USceneComponent* proceduralMeshRef);
	UFUNCTION(BlueprintCallable, Category = "ProceduralActor")
		void CopyDynamic(UProceduralMeshComponent* toCopy, UProceduralMeshComponent* toCopyInto);
	
	
};
