// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "BaseTools.h"
#include "CookGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class HELLOVR_API UCookGameStateBase : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FResetStruct> resetLocations;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<ABaseIngredients*> ingredients;

	UFUNCTION(BlueprintCallable)
		static UCookGameStateBase* Get();
	//UFUNCTION(BlueprintCallable)
	//	int GetAmount(ABaseIngredients* ingredient);

public:
	void addItem(AActor* object);
	void removeItem(AActor* object);
	void addIngredient(ABaseIngredients* object);
	void removeIngredient(ABaseIngredients* object);
};
