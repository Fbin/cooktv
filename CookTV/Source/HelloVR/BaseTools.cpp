// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseTools.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "CookGameStateBase.h"

void ABaseTools::PostInitProperties()
{
	Super::PostInitProperties();
	if (StaticMeshComponent)
	{
		StaticMeshComponent->SetSimulatePhysics(true);
	}
}

void ABaseTools::BeginPlay()
{
	Super::BeginPlay();
	UCookGameStateBase* gamestateRef = UCookGameStateBase::Get();
	gamestateRef->addItem(this);
	StaticMeshComponent->bGenerateOverlapEvents = true;
	StaticMeshComponent->BodyInstance.bUseCCD = true;
	StaticMeshComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Block);
}

void ABaseTools::Destroyed()
{
	Super::Destroyed();
	UCookGameStateBase* gamestateRef = UCookGameStateBase::Get();
	gamestateRef->removeItem(this);
}

//void DrawDebugCircle2(const UWorld* InWorld, const FMatrix& TransformMatrix, float Radius, int32 Segments, const FColor& Color, bool bPersistentLines = false, float LifeTime = -1.f, uint8 DepthPriority = 0, float Thickness = 0.f, bool bDrawAxis = true) {}

FVector ABaseTools::GetLowestLocation(FTransform objctTransform, UObject* WorldContextObject, float Radius)
{
	FVector Center = objctTransform.GetLocation();
	FTransform rot = FTransform(FRotator(90, 0, 0));
	FMatrix matrix = (rot*objctTransform).ToMatrixNoScale();
	const UWorld* InWorld=GEngine->GetWorldFromContextObject(WorldContextObject);
	
	//void DrawDebugCircle(const UWorld* InWorld, const FMatrix& TransformMatrix, float Radius, 
						//int32 Segments, const FColor& Color, bool bPersistentLines = false, 
						//float LifeTime = -1.f, uint8 DepthPriority = 0, float Thickness = 0.f, bool bDrawAxis = true) {}
	if (InWorld->WorldType >= EWorldType::Editor)
	{
	::DrawDebugCircle(InWorld, matrix, Radius, 
					  60, FLinearColor::White.ToFColor(true), false, 
					  5.0f, 1, 1.0f, true);
	}
	
	//::DrawDebugCircle(GEngine->GetWorldFromContextObject(WorldContextObject), Center, Radius, NumSegments, LineColor.ToFColor(true), false, Duration, SDPG_World, Thickness, UKismetMathLibrary::TransformLocation(objctTransform, YAxis), UKismetMathLibrary::TransformLocation(objctTransform, ZAxis), bDrawAxis);
	TArray<FVector> circleLocations;
	for (int i = 0; i < 60; i++)
	{
		circleLocations.Add(Radius*FVector(FMath::Cos(i * 2 * PI / 60), FMath::Sin(i * 2 * PI / 60), 0.f));
	}
	matrix = objctTransform.ToMatrixNoScale();
	FVector lowLocation(0,0,10000000);
	for (FVector element : circleLocations)
	{
		element = matrix.TransformPosition(element);
		FVector end = element + FVector(0, 0, -100);
		//::DrawDebugLine(InWorld, element, end, FLinearColor::Green.ToFColor(true), false, 5.0f, 1, 1.0f);

		if (element.Z <= lowLocation.Z)
		{
			lowLocation = element;
		}
		continue;
	}
	FVector end = lowLocation + FVector(0, 0, -100);
	if (InWorld->WorldType >= EWorldType::Editor)
	{
		::DrawDebugLine(InWorld, lowLocation, end, FLinearColor::Red.ToFColor(true), false, 5.0f, 1, 1.0f);
	}
	return lowLocation;
}

FVector ABaseTools::Boiling(FVector locationIn, FTransform potTransform, UObject* WorldContextObject, float range, float boilingState)
{
	FVector bottom = potTransform.GetLocation();
	FVector up = potTransform.GetUnitAxis(EAxis::Z);
	FVector top = up*range + bottom;
	const UWorld* InWorld = GEngine->GetWorldFromContextObject(WorldContextObject);

	//DrawDebugPlane(UObject* WorldContextObject, FPlane const& P, FVector const Loc, 
					//float Size, FLinearColor Color, float LifeTime)
	if (InWorld->WorldType >= EWorldType::Editor)
	{
		::DrawDebugLine(InWorld, bottom, top, FLinearColor::White.ToFColor(true), false, 5.0f, 1, 1.0f);
	}
	float height = (bottom - top).Size();
	FVector locationOut = locationIn + FMath::Clamp((1-boilingState / height), 0.0f, height) * up;
	if (locationOut.Z >= top.Z)
	{
		locationOut.Z = top.Z;
	}
	return locationOut;
}