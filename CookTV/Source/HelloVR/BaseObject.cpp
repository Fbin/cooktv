// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseObject.h"
#include "Log.h"
#include "Utility.h"

// Sets default values
ABaseObject::ABaseObject(const FObjectInitializer& ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("base_staticMesh"));
	StaticMeshComponent->Mobility = EComponentMobility::Movable;
	StaticMeshComponent->bGenerateOverlapEvents = false;
	StaticMeshComponent->bUseDefaultCollision = true;
	RootComponent = Cast<USceneComponent>(StaticMeshComponent);

}

void ABaseObject::PostInitProperties()
{
	Super::PostInitProperties();
}

// Called when the game starts or when spawned
void ABaseObject::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABaseObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseObject::Grab_Implementation(USceneComponent* handReference)
{
	//UE_LOG(Debug, Warning, TEXT("Grab_Implementation missing: %s"), *GetName());
}

void ABaseObject::Release_Implementation()
{
	//UE_LOG(Debug, Warning, TEXT("Release_Implementation missing: %s"), *GetName());
};

void ABaseObject::Interact_Implementation(AActor* user, USceneComponent* handReference)
{
	//bToggleInteract = !bToggleInteract;
};

void ABaseObject::PickUp_Implementation(USceneComponent* handReference)
{
	Utility::PickUp(StaticMeshComponent, Cast<USceneComponent>(StaticMeshComponent), handReference);
}

void ABaseObject::Drop_Implementation()
{
	Utility::Drop(StaticMeshComponent);
};
