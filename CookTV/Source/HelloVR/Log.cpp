// Fill out your copyright notice in the Description page of Project Settings.

#include "Log.h"
#include "HelloVR.h"

DEFINE_LOG_CATEGORY(Debug);

void ULogger::DebugLog(FString message)
{
	UE_LOG(Debug, Warning, TEXT(":> %s"), *message);

}

/*void AActor::DestroyComponent(UActorComponent* Component)
{
	// If its a valid component, and we own it, destroy it
	if (Component && Component->GetOwner() == this)
	{
		Component->DestroyComponent();
	}
}*/
