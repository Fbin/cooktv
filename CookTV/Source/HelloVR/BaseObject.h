// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ObjectMovementInterface.h"
#include "BaseObject.generated.h"

class UMeshComponent;
class UMaterialInterface;

UENUM(BlueprintType)
enum class EObjectTypes : uint8
{
	PickUp 	UMETA(DisplayName = "PickUp"),
	Grab 	UMETA(DisplayName = "Grab"),
	Interact	UMETA(DisplayName = "Interact")
};

UCLASS()
class HELLOVR_API ABaseObject : public AActor, public IObjectMovementInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseObject(const FObjectInitializer& ObjectInitializer);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
		EObjectTypes ObjectType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
		UStaticMeshComponent* StaticMeshComponent;
	UPROPERTY(EditAnywhere, Category = Basic)
		bool bToggleInteract=false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
		UMaterialInterface* material;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void PostInitProperties() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Grab_Implementation(USceneComponent* handReference) override;
	virtual void PickUp_Implementation(USceneComponent* attachTo) override;
	virtual void Drop_Implementation() override;
	virtual void Release_Implementation() override;
	virtual void Interact_Implementation(AActor* user, USceneComponent* handReference) override;
};
