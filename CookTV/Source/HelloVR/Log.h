// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Log.generated.h"

class AActor;

//General Log
DECLARE_LOG_CATEGORY_EXTERN(Debug, Log, All);

/**
 * 
 */
UCLASS()
class HELLOVR_API ULogger : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	
	UFUNCTION(BlueprintCallable, Category = "Debug"/*, meta = (CompactNodeTitle = "LOG")*/)
	static void DebugLog(FString message);

	/*UFUNCTION(BlueprintCallable, Category = "Debug")
		void DestroyComponent(UActorComponent* Component);*/

};

