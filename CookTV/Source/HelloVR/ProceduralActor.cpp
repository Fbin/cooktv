// Fill out your copyright notice in the Description page of Project Settings.

#include "ProceduralActor.h"
#include "PhysicsEngine/BodySetup.h"
#include "KismetProceduralMeshLibrary.h"

AProceduralActor::AProceduralActor(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	ProceduralMesh = ObjectInitializer.CreateDefaultSubobject<UProceduralMeshComponent>(this, TEXT("proc_mesh"));
	ProceduralMesh->bUseComplexAsSimpleCollision = false;
}

void AProceduralActor::PostInitProperties()
{
	Super::PostInitProperties();

	/*if (StaticMeshComponent)
	{
		UKismetProceduralMeshLibrary::CopyProceduralMeshFromStaticMeshComponent(StaticMeshComponent, 0, ProceduralMesh, true);
		ReRoot(ProceduralMesh);
		//StaticMeshComponent->Deactivate();
		//StaticMeshComponent->SetStaticMesh(nullptr);
	}*/
}

void AProceduralActor::BeginPlay()
{
	Super::BeginPlay();

	if (StaticMeshComponent)
	{
		UKismetProceduralMeshLibrary::CopyProceduralMeshFromStaticMeshComponent(StaticMeshComponent, 0, ProceduralMesh, true);
		//ReRoot(ProceduralMesh);
		//StaticMeshComponent->Deactivate();

		ProceduralMesh->bUseComplexAsSimpleCollision = false;
		ProceduralMesh->SetCollisionObjectType(ECC_GameTraceChannel1);
		ProceduralMesh->SetSimulatePhysics(true);


		ProceduralMesh->SetWorldTransform(StaticMeshComponent->GetComponentToWorld(),false,nullptr,ETeleportType::TeleportPhysics);
		StaticMeshComponent->SetStaticMesh(nullptr);
	}
}

void AProceduralActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (ProceduralMesh->GetComponentLocation().Z <= 0)
	{
		Destroy();
	}
}

void AProceduralActor::ReRoot(USceneComponent* proceduralMeshRef)
{
	RootComponent = proceduralMeshRef;
}

void AProceduralActor::CopyDynamic(UProceduralMeshComponent* toCopy, UProceduralMeshComponent* toCopyInto)
{
	toCopy->bAllowAnyoneToDestroyMe = true;

	TArray<FProcMeshSection> OtherSections;
	TArray<UMaterialInterface*> OtherMaterials;


	TArray< TArray<FVector> > OtherSlicedCollision;
	UBodySetup* ProcMeshBodySetup = toCopy->GetBodySetup();
	for (int32 ConvexIndex = 0; ConvexIndex < ProcMeshBodySetup->AggGeom.ConvexElems.Num(); ConvexIndex++)
	{
		FKConvexElem& BaseConvex = ProcMeshBodySetup->AggGeom.ConvexElems[ConvexIndex];
		OtherSlicedCollision.Add(BaseConvex.VertexData);
	}

	// Set transform to match source component
	toCopyInto->SetWorldTransform(toCopy->GetComponentTransform());

	for (int32 SectionIndex = 0; SectionIndex < toCopy->GetNumSections(); SectionIndex++)
	{

		OtherSections.Add(*toCopy->GetProcMeshSection(SectionIndex));
		OtherMaterials.Add(toCopy->GetMaterial(SectionIndex));

	}
	// Add each section of geometry
	for (int32 SectionIndex = 0; SectionIndex < OtherSections.Num(); SectionIndex++)
	{
		toCopyInto->SetProcMeshSection(SectionIndex, OtherSections[SectionIndex]);
		toCopyInto->SetMaterial(SectionIndex, OtherMaterials[SectionIndex]);
	}
	// Copy collision settings from input mesh
	toCopyInto->SetCollisionProfileName(toCopy->GetCollisionProfileName());
	toCopyInto->SetCollisionEnabled(toCopy->GetCollisionEnabled());
	toCopyInto->bUseComplexAsSimpleCollision = toCopy->bUseComplexAsSimpleCollision;

	// Assign sliced collision
	toCopyInto->SetCollisionConvexMeshes(OtherSlicedCollision);

	// Finally register
	toCopyInto->RegisterComponent();
	toCopyInto->SetMobility(EComponentMobility::Movable);
}