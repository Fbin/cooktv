// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ObjectMovementInterface.generated.h"

/**
 * 
 */
UINTERFACE(Blueprintable)
class UObjectMovementInterface : public UInterface
{
	GENERATED_BODY()
};

class HELLOVR_API IObjectMovementInterface
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ObjectInterface")
		void PickUp(USceneComponent* attachTo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ObjectInterface")
		void Drop();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ObjectInterface")
		void Grab(USceneComponent* handReference);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ObjectInterface")
		void Release();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ObjectInterface")
		void Interact(AActor* user, USceneComponent* handReference);
};
