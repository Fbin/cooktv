// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseObject.h"
#include "BaseIngredients.generated.h"


UENUM(BlueprintType)
enum class EIngredientType : uint8
{
	Roastable 	UMETA(DisplayName = "Roastable"),
	Boilable 	UMETA(DisplayName = "Boilable"),
	Bakeable	UMETA(DisplayName = "Bakeable")
};


/**
 * 
 */
UCLASS()
class HELLOVR_API ABaseIngredients : public ABaseObject
{
	GENERATED_BODY()
private:
	UClass* lClass;
public:
	UClass* GetLClass();
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
	//	EIngredientType IngredientType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
		bool boilable;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
		bool bakeable;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Basic)
		bool roastable;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BasicMaterials)
		UMaterialInterface* baseRoastMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BasicMaterials)
		UMaterialInterface* baseBoilMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BasicMaterials)
		UMaterialInterface* baseBakeMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BasicMaterials)
		UMaterialInterface* baseBurntMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BasicMaterials)
		UMaterialInterface* baseOverboiledMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BasicMaterials)
		UMaterialInterface* baseOverbakedMaterial;
	UPROPERTY(BlueprintReadWrite, Category = OnlyForBP)
		UMaterialInstanceDynamic* baseMaterial; 
	UPROPERTY(BlueprintReadWrite, Category = OnlyForBP)
		UMaterialInstanceDynamic* bakingMaterial;
	UPROPERTY(BlueprintReadWrite, Category = OnlyForBP)
		UMaterialInstanceDynamic* overBakedMaterial;
	UPROPERTY(BlueprintReadWrite, Category = OnlyForBP)
		UMaterialInstanceDynamic* boilingMaterial;
	UPROPERTY(BlueprintReadWrite, Category = OnlyForBP)
		UMaterialInstanceDynamic* overBoiledMaterial;
	UPROPERTY(BlueprintReadWrite, Category = OnlyForBP)
		UMaterialInstanceDynamic* roastingMaterial;
	UPROPERTY(BlueprintReadWrite, Category = OnlyForBP)
		UMaterialInstanceDynamic* overRoastedMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Roasting)
		float roastHeat;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Roasting)
		float roastTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Roasting)
		float roastTimeLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Roasting)
		float goodRoastTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Roasting)
		float goodRoastTimeLeft;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Boiling)
	//	float boilingHeat;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Boiling)
		float boilingTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Boiling)
		float boilingTimeLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Boiling)
		float goodBoilTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Boiling)
		float goodBoilTimeLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Baking)
		float bakeHeat;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Baking)
		float bakeTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Baking)
		float bakeTimeLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Baking)
		float goodBakeTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Baking)
		float goodBakeTimeLeft;
protected:
	virtual void BeginPlay() override;
	virtual void PostInitProperties() override;
	virtual void Destroyed() override;
};
